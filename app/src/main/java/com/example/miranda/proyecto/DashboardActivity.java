package com.example.miranda.proyecto;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.miranda.proyecto.adapter.ViewPagerAdapter;

public class DashboardActivity extends AppCompatActivity {


    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);




        SharedPreferences preferences=getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        int user_id = preferences.getInt("user_id",0);


        // int user_id=preferences.getString("user_id","");
        //if (user_id.isEmpty())

        if (user_id==0)
        {
            //Usuario no Logueado
            Intent intent=new Intent(this,LoginActivity.class);
            startActivity(intent);

        }
        else {

            //Toast.makeText(this, "user_id: " + user_id, Toast.LENGTH_LONG).show();

        }



        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0);
        }
        tabLayout = (TabLayout)findViewById(R.id.tab_layout);

        tabLayout.addTab(tabLayout.newTab().setText("Posts"));
         tabLayout.addTab(tabLayout.newTab().setText("Users"));

        viewPager = (ViewPager)findViewById(R.id.view_pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);



        viewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    @Override

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.dashboard_menu,menu);
        return true;

    }

    //Un overrate que responde a los eventos del menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.logout_option:
               //Toast.makeText(this,"Logout",Toast.LENGTH_LONG).show();
                logout();
                break;
            case R.id.register_option:
                //Toast.makeText(this,"register Menu",Toast.LENGTH_LONG).show();
                register();
                break;
            case R.id.action_refresh:
                update();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void update() {
        Toast.makeText(this, "Refresh" , Toast.LENGTH_LONG).show();
        Intent refresh = new Intent(this, DashboardActivity.class);
        startActivity(refresh);//Start the same Activity
        finish();
    }

    private void logout() {
        SharedPreferences preferences=getSharedPreferences(getString(R.string.app_name),//Nombre Archivo
                Context.MODE_PRIVATE);//Solo Nuestra App Puede Ver Esto
        SharedPreferences.Editor editor=preferences.edit();//Abrir el archivo para escritura
        editor.remove("user_id");//Elimina el username

        //editor.remove("username");//Elimina el username
        editor.commit();//Guarda Cambios

        Intent intent=new Intent(this, DashboardActivity.class);
        startActivity(intent);

    }

    private void register()
    {
        Intent intent2=new Intent(this, RegisterActivity.class);
        startActivity(intent2);

    }
}
