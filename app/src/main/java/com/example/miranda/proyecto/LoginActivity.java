package com.example.miranda.proyecto;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.miranda.proyecto.model.User;
import com.example.miranda.proyecto.network.RetrieveLoginAsyncTask;

public class LoginActivity extends AppCompatActivity {
    private EditText username;
    private EditText password;

    private EditText usernameEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Para Login Basico
        username =(EditText)findViewById(R.id.TxtUserName);
        password =(EditText)findViewById(R.id.TxtPasword);
        //Para Login con Servicio
        usernameEditText = (EditText)findViewById(R.id.TxtUserName);
        passwordEditText = (EditText)findViewById(R.id.TxtPasword);

        usernameEditText.setText("example1");
        passwordEditText.setText("foobar!1");

    }

    public void loginClick(View view) {
        //Login Basico
        /*
        //Mensaje de Depuracion
        Log.d("LoginActivity","You clicked me!");
        Log.d("LoginActivity","Username: "+username.getText());
        Log.d("LoginActivity","Pasword: "+password.getText());

        String rightUserName="admin";
        String rightPassword="root";

        if (rightUserName.equals(username.getText().toString())&& rightPassword.equals(password.getText().toString()))
        {
            //Login Ok
            //Contexto componente en Android
            //El contexto Soy Yo (this)
            Toast.makeText(this, R.string.login_succes,Toast.LENGTH_LONG).show();

            //Guardando en SharedPreferences   //Solo Permite Datos Primitivos
            SharedPreferences preferences=getSharedPreferences(getString(R.string.app_name),//Nombre Archivo
                    Context.MODE_PRIVATE);//Solo Nuestra App Puede Ver Esto
            SharedPreferences.Editor editor=preferences.edit();//Abrir el archivo para escritura
            editor.putString("username",username.getText().toString());
            editor.commit();//Guarda Cambios

            //Llamamos a la Ventana Dashboard
            Intent intent=new Intent(this, DashboardActivity.class);
            startActivity(intent);
        }
        else {
            Toast.makeText(this, R.string.login_failed,Toast.LENGTH_LONG).show();
        }
        */
        //Login con Servicio
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();


        User user = new User();
        user.setUsername(username);
        user.setPassword(password);





        RetrieveLoginAsyncTask task = new RetrieveLoginAsyncTask(this);
        task.execute(user);
    }

}
