package com.example.miranda.proyecto;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.miranda.proyecto.adapter.UserAdapter;
import com.example.miranda.proyecto.model.User;
import com.example.miranda.proyecto.network.RetrieveUsersAsyncTask;

/**
 * Created by Miranda on 26/06/2016.
 */
public class UserFragment extends Fragment {
    private ListView listView;
    private UserAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user, container, false);
        // Inflate the layout for this fragment
        listView = (ListView)view.findViewById(R.id.users_list_view);
        adapter = new UserAdapter(getActivity());
        listView.setAdapter(adapter);

        //Para Recuperar de la Lista
        //Añadido selecciona de la lista de post
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User p = adapter.getItem(position);

                AlertDialog alertDialog=new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle(getString(R.string.detail));

                alertDialog.setMessage(getString(R.string.user_name)+":\n"+p.getUsername()+"\n\n"+getString(R.string.email)+":\n"+p.getEmail());
                alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                alertDialog.show();

                //Toast.makeText(getActivity(), p.getTitle(), Toast.LENGTH_LONG).show();
            }
        });






        RetrieveUsersAsyncTask task = new RetrieveUsersAsyncTask(this);
        task.execute();

        return view;
    }

    public UserAdapter getAdapter() {
        return adapter;
    }
}
