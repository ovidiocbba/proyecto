package com.example.miranda.proyecto.network;



import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.miranda.proyecto.DashboardActivity;
import com.example.miranda.proyecto.R;
import com.example.miranda.proyecto.RegisterActivity;
import com.example.miranda.proyecto.model.Post;
import com.example.miranda.proyecto.model.User;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Miranda on 10/07/2016.
 */
public class RetrieveRegisterAsyncTask extends AsyncTask<Post, Void, Post> {

    private RegisterActivity activity;

    public RetrieveRegisterAsyncTask(RegisterActivity activity) {
        this.activity = activity;
    }

    @Override
    protected Post doInBackground(Post... params) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://dip-androiducbv2.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PostService service = retrofit.create(PostService.class);

        Call<Post> call = service.register(params[0]);

        try {
            Response<Post> response = call.execute();
            Post post = response.body();

            return post;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Post post) {
        if (post == null) {
            // Codigo de error
            Toast.makeText(activity, "Could not register", Toast.LENGTH_LONG).show();


        } else {

            Toast.makeText(activity,
                    "Registered Successfully",
                    Toast.LENGTH_SHORT).show();
            activity.finish();
            /*

            Intent intent=new Intent(activity,DashboardActivity.class);
            activity.startActivity(intent);
            */
        }
    }

}
