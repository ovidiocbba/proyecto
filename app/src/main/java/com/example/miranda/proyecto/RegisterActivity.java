package com.example.miranda.proyecto;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.miranda.proyecto.model.Post;
import com.example.miranda.proyecto.model.User;
import com.example.miranda.proyecto.network.RetrieveLoginAsyncTask;
import com.example.miranda.proyecto.network.RetrieveRegisterAsyncTask;

public class RegisterActivity extends AppCompatActivity {

    private EditText titleEditText;
    private EditText contentEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //Para Login con Servicio
        titleEditText = (EditText)findViewById(R.id.TxtTitle);
        contentEditText = (EditText)findViewById(R.id.TxtContent);
    }

    public void btnSavePost(View view) {

       String title = titleEditText.getText().toString();
       String content = contentEditText.getText().toString();


        if (TextUtils.isEmpty(title)&&TextUtils.isEmpty(content))
        {
            Toast.makeText(this, "Check has empty fields",
                    Toast.LENGTH_LONG).show();
        }else
        {
            Post post = new Post();
            post.setTitle(title);
            post.setContent(content);
            RetrieveRegisterAsyncTask task = new RetrieveRegisterAsyncTask(this);
            task.execute(post);
        }
/*
        Toast.makeText(this,
                "Title: "+title+"Content: "+content,
                Toast.LENGTH_SHORT).show();
                */



    }
}
