package com.example.miranda.proyecto.network;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;


import com.example.miranda.proyecto.DashboardActivity;
import com.example.miranda.proyecto.LoginActivity;
import com.example.miranda.proyecto.R;
import com.example.miranda.proyecto.model.User;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Miranda on 27/06/2016.
 */
public class RetrieveLoginAsyncTask extends AsyncTask<User, Void, User> {


    private LoginActivity activity;

    public RetrieveLoginAsyncTask(LoginActivity activity) {
        this.activity = activity;
    }

    @Override
    protected User doInBackground(User... params) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://dip-androiducbv2.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PostService service = retrofit.create(PostService.class);
        Call<User> call = service.login(params[0]);
        try {
            Response<User> response = call.execute();
            User user = response.body();

            return user;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(User user) {
        if (user == null) {
            // Codigo de error
            Toast.makeText(activity, "Login Unsuccessful", Toast.LENGTH_LONG).show();


        } else {
            // Guardar al usuario en las preferencias
            SharedPreferences sharedPreferences = activity.getSharedPreferences(
                    activity.getString(R.string.app_name),
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            //user.getId()
            editor.putInt("user_id", user.getId());

           // editor.putString("user_id", (user.getId().toString()));
            editor.commit();//Guarda Cambios

            /*
            Toast.makeText(activity,
                    "Login successful, username: " + user.getUsername(),
                    Toast.LENGTH_LONG).show();
                    */
            Toast.makeText(activity,
                    "Login Successful",
                    Toast.LENGTH_SHORT).show();

            Intent intent=new Intent(activity,DashboardActivity.class);
            activity.startActivity(intent);
        }
    }



}