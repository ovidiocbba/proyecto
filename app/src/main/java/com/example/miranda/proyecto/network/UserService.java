package com.example.miranda.proyecto.network;

import com.example.miranda.proyecto.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Miranda on 26/06/2016.
 */
public interface UserService {
    @GET("users.json")
    Call<List<User>> getUsers();
}