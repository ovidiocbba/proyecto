package com.example.miranda.proyecto.network;

import com.example.miranda.proyecto.model.Post;
import com.example.miranda.proyecto.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Miranda on 26/06/2016.
 */
public interface PostService {
    @GET("posts?user_id=1")
    Call<List<Post>> getPosts();

    @POST("/login.json")
    Call<User> login(@Body User user);

    @POST("/posts?user_id=1")
    Call<Post> register(@Body Post post);
}
