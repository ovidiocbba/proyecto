package com.example.miranda.proyecto;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.miranda.proyecto.adapter.PostAdapter;
import com.example.miranda.proyecto.model.Post;
import com.example.miranda.proyecto.network.RetrievePostsAsyncTask;

/**
 * Created by Miranda on 26/06/2016.
 */
public class PostFragment extends Fragment {
    private ListView listView;
    private PostAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        // Inflate the layout for this fragment
        listView = (ListView)view.findViewById(R.id.posts_list_view);
        adapter = new PostAdapter(getActivity());
        listView.setAdapter(adapter);

        //Añadido selecciona de la lista de post
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Post p = adapter.getItem(position);

                AlertDialog alertDialog=new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle(getString(R.string.detail));
                //alertDialog.setTitle("Title: "+p.getTitle());
                alertDialog.setMessage(getString(R.string.Title)+":\n"+p.getTitle()+"\n\n"+getString(R.string.Content)+":\n"+p.getContent());
                alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                alertDialog.show();

                //Toast.makeText(getActivity(), p.getTitle(), Toast.LENGTH_LONG).show();
            }
        });

        RetrievePostsAsyncTask task = new RetrievePostsAsyncTask(this);
        task.execute();

        return view;
    }

    public PostAdapter getAdapter() {
        return adapter;
    }
}
